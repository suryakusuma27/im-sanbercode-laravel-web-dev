<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    //
    public function create()
    {
        return view("cast.create");
    }
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|min:3',
            'umur' => 'required|min:1',
            'bio' => 'required',

        ]);
        $name = $request->input('nama');
        $umur = $request->input('umur');
        $bio = $request->input('bio');

        DB::table('cast')->insert(compact('name', 'umur', 'bio'));
        return redirect('/cast');
    }
    public function index()
    {
        $cast = DB::table("cast")->get();
        return view("cast.tampil", ["casts" => $cast]);
    }

    public function show($id)
    {
        $data = DB::table("cast")->find($id);
        return view("cast.detail", ["cast" => $data]);
    }

    public function edit($id)
    {
        $data = DB::table("cast")->find($id);
        return view("cast.edit", ["cast" => $data]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|min:3',
            'umur' => 'required|min:1',
            'bio' => 'required',
        ]);

        DB::table('cast')->where('id', $id)->update(['name' => $request->input('nama'), 'umur' =>
        $request->input('umur'), 'bio' => $request->input('bio')]);

        return redirect('/cast');
    }

    public function destroy($id){
        DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}
