@extends('layouts.master')

@section('title')
    Detail Cast
@endsection

@section('content')
    <div class="card">
        <div class="card-body d-flex flex-column">
            <h5 class="card-title">{{ $cast->name }}</h5>
            <h6 class="card-text text-muted">{{ $cast->umur }} Tahun</h6>
            <p class="card-text">{{ $cast->bio }}</p>
        </div>
    </div>
@endsection
