@extends('layouts.master')

@section('title')
    Cast
@endsection

@section('content')
    <a href="/cast/create">
        <button class="btn btn-primary">Tambah Cast</button>
    </a>

    <table class="table table-hover table-bordered mt-3">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($casts as $key => $cast)
                <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>{{ $cast->name }}</td>
                    <td>{{ $cast->umur }}</td>
                    <td>{{ $cast->bio }}</td>
                    <td>
                        <div class="d-flex border-0" style="gap: 8px">
                            <a href="/cast/{{ $cast->id }}">
                                <button class="btn btn-info">Detail</button>
                            </a>
                            <a href="/cast/{{ $cast->id }}/edit">
                                <button class="btn btn-warning">Edit</button>
                            </a>
                            <form action="/cast/{{ $cast->id }}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </div>
                    </td>
                </tr>
            @empty
            @endforelse

        </tbody>
    </table>
@endsection
