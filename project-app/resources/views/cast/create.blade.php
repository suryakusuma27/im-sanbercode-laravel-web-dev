@extends('layouts.master')

@section('title')
    Tambah Cast
@endsection

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" id="nama" name="nama"placeholder="Masukkan Nama">
        </div>
        <div class="form-group">
            <label>Umur</label>
            <input type="number" class="form-control" id="umur" name="umur" placeholder="Masukkan Umur">
        </div>
        <div class="form-group">
            <label>Bio</label>
            <textarea type="number" class="form-control" id="bio" name="bio" placeholder="Masukkan Bio"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
