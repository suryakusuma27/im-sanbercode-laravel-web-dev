@extends('layouts.master')

@section('title')
    Edit Cast
@endsection

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="/cast/{{ $cast->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" id="nama" name="nama" value="{{ $cast->name }}"
                placeholder="Masukkan Nama">
        </div>
        <div class="form-group">
            <label>Umur</label>
            <input type="number" class="form-control" id="umur" name="umur" value="{{ $cast->umur }}"
                placeholder="Masukkan Umur">
        </div>
        <div class="form-group">
            <label>Bio</label>
            <textarea type="number" class="form-control" id="bio" name="bio" placeholder="Masukkan Bio">{{ $cast->bio }}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
