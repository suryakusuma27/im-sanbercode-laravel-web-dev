<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label for="fname">First name:</label><br><br>
        <input type="text" id="fname" name="fname" required><br><br>
        <label for="lname">Last name:</label><br><br>
        <input type="text" id="lname" name="lname"><br><br>
        <label for="gender">Gender:</label><br><br>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label><br><br>
        <label for="nationality">Nationality:</label><br><br>
        <select id="nationality" name="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="other">Other</option>
        </select><br><br>
        <label for="lang">Language Spoken:</label><br><br>
        <input type="checkbox" id="bahasaindonesia" name="lang" value="bahasaindonesia">
        <label for="bahasaindonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" id="english" name="lang" value="english">
        <label for="english">English</label><br>
        <input type="checkbox" id="otherlang" name="lang" value="otherlang">
        <label for="otherlang">Other</label><br><br>
        <label for="bio">Bio:</label><br><br>
        <textarea id="bio" name="bio" rows="4" cols="50"></textarea><br><br>
        <input type="submit" value="Submit">
    </form>
</body>


</html>
