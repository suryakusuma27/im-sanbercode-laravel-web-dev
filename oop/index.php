<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    require_once "Animal.php";
    require_once "Ape.php";
    require_once "Frog.php";

    function printArray($className)
    {
        $arr = ["name", "legs", "cold_blooded"];
        foreach ($arr as $k) {
            echo str_replace('_', ' ', ucfirst($k)) . " : " . $className->$k;
            echo "<br/>";
        }
    }

    // untuk animal
    $sheep = new Animal("shaun");
    printArray($sheep);
    echo "<br/>";

    // untuk kera
    $sungokong = new Ape("kera sakti");
    printArray($sungokong);
    echo "Yell : " . $sungokong->yell();
    echo "<br/><br/>";

    // untuk kodok
    $kodok = new Frog("buduk");
    printArray($kodok);
    echo "Jump : " . $kodok->jump();
    echo "<br/><br/>";

    ?>
</body>

</html>